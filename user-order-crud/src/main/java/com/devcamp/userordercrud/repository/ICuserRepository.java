package com.devcamp.userordercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userordercrud.model.CUser;

public interface ICuserRepository extends JpaRepository<CUser, Long> {
    
}
