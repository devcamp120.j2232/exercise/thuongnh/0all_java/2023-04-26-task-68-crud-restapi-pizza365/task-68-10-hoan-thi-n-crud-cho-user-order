package com.devcamp.userordercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userordercrud.model.COrder;

public interface IOrderReponsitory extends JpaRepository<COrder, Long>{
    
}
