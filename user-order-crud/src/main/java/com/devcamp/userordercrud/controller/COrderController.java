package com.devcamp.userordercrud.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userordercrud.model.COrder;
import com.devcamp.userordercrud.repository.IOrderReponsitory;

@RestController
@CrossOrigin(value = "*", maxAge = -1) // maxAge = -1 không lưu cache vào máy khách
@RequestMapping("/order")
public class COrderController {

    @Autowired
    IOrderReponsitory pIOrderReponsitory;
      // lấy tất cả danh sách user
      @GetMapping("/all")
      public ResponseEntity<Object> getAllUsers() {
          // ResponseEntity<Object> tạo ra đối tượng responseEntity với kiểu dữ liệu trả
          // về là Object
  
          List<COrder> userList = new ArrayList<COrder>();
          pIOrderReponsitory.findAll().forEach(userList::add);
  
          // kiểm tra xem list có rỗng hãy không . nếu rỗng thì báo lỗi ..
          if (!userList.isEmpty()) {
              return new ResponseEntity<>(userList, HttpStatus.OK);
  
          } else {
              return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
  
          }
  
      }

       // Viết method get list user: getUserById(), và chạy test trên postman
    @GetMapping("/detail")
    public ResponseEntity<Object> getUserById(@RequestParam(name = "id", required = true) long id) {
        Optional<COrder> orderData = pIOrderReponsitory.findById(id);
        // Optional để giải quyết các vấn đề về truy cập vào các đối tượng có thể null.
        // Nó đóng vai trò là một bao bọc an toàn cho các giá trị có thể không tồn tại,
        // giúp tránh những lỗi NullPointerException khi truy cập vào đối tượng null.
        if (orderData.isPresent()) {
            return new ResponseEntity<>(orderData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

     // 7 Viết method create user: createUser(), và chạy test trên postman
     @PostMapping("/create")
     public ResponseEntity<Object> createUser(@RequestBody COrder orderDataClient) {
 
         try {
             // khởi tạo 1 đối tượng để lưu kết người dùng cần tọa
             COrder _order = new COrder( );
             Date _now = new Date(); // lấy ngày tạo từ hệ thông
             _order.setOrderCode(orderDataClient.getOrderCode());
             _order.setPizzaSize(orderDataClient.getPizzaSize());
             _order.setPizzaType(orderDataClient.getPizzaType());
             _order.setVoucherCode(orderDataClient.getVoucherCode());
             _order.setPrice(orderDataClient.getPrice());
             _order.setPaid(orderDataClient.getPaid());
             _order.setCreated( _now);
             _order.setUpdated(null);
             _order.setUser(orderDataClient.getUser());
             pIOrderReponsitory.save(_order);
             return new ResponseEntity<Object>(_order, HttpStatus.OK);
 
         } catch (Exception e) {
             // TODO: handle exception
             return ResponseEntity.unprocessableEntity()
                     .body("Failed to create specified user" + e.getCause().getCause().getMessage());
 
         }
     }

       // Viết method update user: updateUser(), và chạy test trên postman
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long paramId, @RequestBody COrder orderDataClient) {

        // tìm user theo id trên data base
        Optional<COrder> _orderData = pIOrderReponsitory.findById(paramId);
        // kiểm tra có null hay k ,, true là khác null
        if (_orderData.isPresent()) {
            // get(); là của Optional để lấy giá tri của đối tượng
            COrder _order = _orderData.get(); // lấy giá trị của _orderData gán cho _order
            Date _now = new Date(); // lấy ngày tạo từ hệ thông
            _order.setOrderCode(orderDataClient.getOrderCode());
            _order.setPizzaSize(orderDataClient.getPizzaSize());
            _order.setPizzaType(orderDataClient.getPizzaType());
            _order.setVoucherCode(orderDataClient.getVoucherCode());
            _order.setPrice(orderDataClient.getPrice());
            _order.setPaid(orderDataClient.getPaid());
            _order.setUpdated(_now);
            _order.setUser(orderDataClient.getUser());

            try {
                return ResponseEntity.ok(pIOrderReponsitory.save(_order));

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

     // Viết method delete user: deleteUser(), và chạy test trên postman Chú ý:
    // delete user thì xóa luôn all orders thuộc user đó
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
        Optional<COrder> _userData = pIOrderReponsitory.findById(id);
        // nếu khác null
        if (_userData.isPresent()) {
            try {
                pIOrderReponsitory.deleteById(id);
                return new ResponseEntity<Object>("đã xóa người dùng có id là  " + id, HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }

        } else {
            return new ResponseEntity<Object>("không tìm thấy user", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    
}
